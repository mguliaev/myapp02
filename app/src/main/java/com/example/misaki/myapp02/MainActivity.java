package com.example.misaki.myapp02;

import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textViewHeader1;
    private TextView textViewHeader2;
    private TextView textViewHeader3;
    private TextView textViewHeader4;

    private TextView textLine1_Right;
    private TextView textLine2_Right;
    private TextView textLine3_Right;

    private TableLayout tableLayout;

    private Button buttonView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewHeader1 = (TextView)findViewById(R.id.textHeader1);
        textViewHeader2 = (TextView)findViewById(R.id.textHeader2);
        textViewHeader3 = (TextView)findViewById(R.id.textHeader3);
        textViewHeader4 = (TextView)findViewById(R.id.textHeader4);

        textLine1_Right = (TextView)findViewById(R.id.textLine1_Right);
        textLine2_Right = (TextView)findViewById(R.id.textLine2_Right);
        textLine3_Right = (TextView)findViewById(R.id.textLine3_Right);

        textViewHeader1.setText("Кредит Выгодный");
        textViewHeader2.setText("100500 Р");
        textViewHeader3.setText("Осталось выплатить");
        textViewHeader4.setText("55800 Р");

        textLine1_Right.setText("06.06.2018");
        textLine2_Right.setText("08.07.2018");
        textLine3_Right.setText("8500 Р");

        buttonView = (Button)findViewById(R.id.buttonView);
        buttonView.setOnClickListener(buttonViewListener);

    }

    private OnClickListener buttonViewListener = new OnClickListener() {
        public void onClick(View v) {
            if(tableLayout==null) {
                LinearLayout layoutBottom = (LinearLayout)findViewById(R.id.layoutBottom);

//        Button buttonView = new Button(MainActivity.this);
//        buttonView.setText("View");
//        layoutBottom.addView(buttonView);

                tableLayout = new TableLayout(MainActivity.this);
                TableRow rowTitle = new TableRow(MainActivity.this);
                TextView title1 = new TextView(MainActivity.this);
                TextView title2 = new TextView(MainActivity.this);
                TextView title3 = new TextView(MainActivity.this);
                title1.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.0f));
                title2.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.0f));
                title3.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.0f));
                title1.setTextSize(24);
                title2.setTextSize(24);
                title3.setTextSize(24);
                title1.setBackgroundColor(0x80C8C8);
                title1.setText("Статус");
                title2.setText("Дата платежа");
                title3.setText("Сумма");
                rowTitle.addView(title1);
                rowTitle.addView(title2);
                rowTitle.addView(title3);

                tableLayout.addView(rowTitle);

                String[] status = {"+", "+", "x", "-"};
                String[] date = {"01.02.2018", "02.03.2018", "03.04.2018", "04.05.2018"};
                String[] summ = { "8501 Р", "8502 Р", "8503 Р", "8504 Р" };

                for(int i=0; i<status.length; i++) {
                    TableRow row = new TableRow(MainActivity.this);
                    //TextView col1 = new TextView(MainActivity.this);
                    ImageView col1 = new ImageView(MainActivity.this);
                    TextView col2 = new TextView(MainActivity.this);
                    TextView col3 = new TextView(MainActivity.this);
                    col1.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f));
                    col2.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f));
                    col3.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f));
                    //col1.setText(status[i]);
                    switch (status[i]) {
                        case "+":
                            col1.setImageResource(R.drawable.ic_check_green);
                            break;
                        case "x":
                            col1.setImageResource(R.drawable.ic_clear_red);
                            break;
                        default:
                            col1.setImageResource(R.drawable.ic_remove_gray);
                    }

                    col2.setText(date[i]);
                    col3.setText(summ[i]);
                    row.addView(col1);
                    row.addView(col2);
                    row.addView(col3);
                    row.setBackgroundColor(0xFF0000);
                    tableLayout.addView(row);
                }

                layoutBottom.addView(tableLayout);
            } else {

            }
        }
    };

}
